package com.mysql.core.pages;

import com.geccocrawler.gecco.GeccoEngine;
import com.mysql.core.pages.gecco.listener.DataSave;

/**
 * @author WeiHui-Z
 * @since 2021-11-04 19:22
 */
public class ApplicationMain {

	private static final String filePath = "D://123.md";

	public static void main(String[] args) {
		GeccoEngine.create()
				.setEventListener(new DataSave(filePath))
				.classpath("com.mysql.core.pages.gecco")
				.seed("http://mysql.taobao.org/monthly/")
				.loop(false)
				.thread(20)
				.run();
	}

}
