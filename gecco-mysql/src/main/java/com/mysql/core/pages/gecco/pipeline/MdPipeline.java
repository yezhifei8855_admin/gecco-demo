package com.mysql.core.pages.gecco.pipeline;

import com.geccocrawler.gecco.annotation.PipelineName;
import com.geccocrawler.gecco.pipeline.Pipeline;
import com.mysql.core.pages.data.DataContainer;
import com.mysql.core.pages.gecco.model.SecondPage;
import lombok.extern.slf4j.Slf4j;

/**
 * @author WeiHui-Z
 * @since 2021-11-04 17:09
 */
@Slf4j
@PipelineName("mdPipeline")
public class MdPipeline implements Pipeline<SecondPage> {

	@Override
	public void process(final SecondPage bean) {
		DataContainer.addData(bean);
	}

}
